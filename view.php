<?php
require __DIR__ . '/vendor/autoload.php';

$s = new \Ministats\Store;
(array_diff(($r = ['action', 'tag']), array_keys(($g = $_GET))) == [] && array_filter($g) == $g) ? $s->{$g[$r[0]]}($g[$r[1]]) : 0;

$c = $s->values();
echo '<table><tr><th>Tag Name</th><th>Count</th></tr>'.
	implode(array_map(function ($k, $v) {return "<tr><td>$k</td><td>$v</td></tr>";}, array_keys($c), $c)).
	'</table>';