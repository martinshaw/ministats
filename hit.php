<?php
require __DIR__ . '/vendor/autoload.php';

if (empty(($t = $_GET['tag']))) { http_response_code(404); echo "Requires 'tag' input field value"; die(); }

(new \Ministats\Store)->incr($t);