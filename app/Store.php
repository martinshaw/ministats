<?php
namespace Ministats;

class Store {
	protected $c;

	public function __construct () {
		$this->c = new \Predis\Client();
		try { $this->c->ping(); } catch (\Predis\Connection\ConnectionException $e) { http_response_code(404); echo "Cannot connect to Redis service"; die(); }
	}

	public function values() {
		$vs = [];
		foreach ($this->c->keys('ministats*') as $t) { $vs[str_replace('ministats:', '', $t)] = $this->c->get($t); }
		return $vs;
	}

	public function __call($mtd, $args) {
		$args[0] = 'ministats:'.$args[0];
		return in_array($mtd, ['incr', 'decr', 'del']) ? call_user_func_array([$this->c, $mtd], $args) : 0;
	}
}